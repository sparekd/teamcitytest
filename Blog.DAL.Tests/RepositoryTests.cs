﻿using System;
using System.Linq;
using Blog.DAL.Infrastructure;
using Blog.DAL.Model;
using Blog.DAL.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TDD.DbTestHelpers.Yaml;
using TDD.DbTestHelpers.Core;

namespace Blog.DAL.Tests
{
    [TestClass]
    public class RepositoryTests : DbBaseTest<BlogFixtures>
    {
	    private BlogRepository _repository;
	    private BlogContext _context;

	    [TestInitialize]
		public void SetuUp()
		{
			_context = new BlogContext();
			_context.Database.CreateIfNotExists();
			_repository = new BlogRepository();
		}

        [TestMethod]
        public void Database_WhenInitializedFromYaml_ContainsOnePost()
        {
            var result = _repository.GetAllPosts();
            Assert.AreEqual(1, result.Count());
        }

        [TestMethod]
        public void AddPost_OnePostInDb_ReturnCorrectNumberOfPosts()
        {
            var post = new Post { Author = "aaa", Content = "xyz" };
            _repository.AddPost(post);
            var result = _repository.GetAllPosts();
            Assert.AreEqual(2, result.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AddPost_WithoutRequiredData_ThrowsException()
        {
            var post = new Post { Author = null, Content = "xyz" };
            _repository.AddPost(post);
        }

		[TestMethod]
		public void AddComment_OneCommentInDb_ReturnCorrectNumberOfComments()
		{
			var comment = new Comment {Content = "hehe", PostId = 1};
			_repository.AddComment(comment);
			var result = _repository.GetCommentsForPost(1);
			Assert.AreEqual(2, result.Count());
		}
    }

    public class BlogFixtures : YamlDbFixture<BlogContext, BlogFixturesModel>
    {
        public BlogFixtures()
        {
			SetYamlFolderName(Environment.CurrentDirectory);
            SetYamlFiles("posts.yml", "comments.yml");
        }
    }

    public class BlogFixturesModel
    {
        public FixtureTable<Post> Posts { get; set; }
	    public FixtureTable<Comment> Comments { get; set; }
    }

}