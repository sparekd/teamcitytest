﻿using System.Collections.Generic;
using System.Linq;
using Blog.DAL.Infrastructure;
using Blog.DAL.Model;
using System;

namespace Blog.DAL.Repository
{
    public class BlogRepository
    {
        private readonly BlogContext _context;

        public BlogRepository()
        {
            _context = new BlogContext();
        }

        public IEnumerable<Post> GetAllPosts()
        {
            return _context.Posts;
        }

        public void AddPost(Post post)
        {
            if (post.Author == null)
                throw new ArgumentException("Author");
            _context.Posts.Add(post);
            _context.SaveChanges();
        }

		public IEnumerable<Comment> GetCommentsForPost(long postId)
		{
			return _context.Comments.Where(x => x.PostId == postId);
		}

		public void AddComment(Comment comment)
		{
			_context.Comments.Add(comment);
			_context.SaveChanges();
		}
    }
}
