﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Blog.DAL.Model
{
	public class Comment
	{
		[Key]
		public long Id { get; set; }

		public long PostId { get; set; }

		public string Content { get; set; }
	}
}
